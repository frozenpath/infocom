<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Staff;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    /**
     * @Route("/department")
     * @Method("GET")
     */
    public function getDepartmentsAction()
    {
        $data = [];
        $i = 0;

        $departments = $this->getDoctrine()->getRepository('AppBundle:Department')
            ->findAll();

        foreach ($departments as $department) {
            $data[$i]['id'] = $department->getId();
            $data[$i]['name'] = $department->getName();
            $i++;
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/department/{id}")
     * @Method("GET")
     * @param int $id
     * @return JsonResponse
     */
    public function getStaffInDepartmentsAction(int $id)
    {
        $data = [];
        $i = 0;

        $staffs = $this->getDoctrine()->getRepository('AppBundle:Staff')
            ->getStaffByDepartment($id);

        /** @var Staff $staff */
        foreach ($staffs as $staff) {
            $data[$i]['id'] = $staff->getId();
            $data[$i]['surname'] = $staff->getSurname();
            $data[$i]['name'] = $staff->getName();
            $data[$i]['patronymic'] = $staff->getPatronymic();
            $data[$i]['pin'] = $staff->getPin();
            $data[$i]['email'] = $staff->getEmail();
            $data[$i]['phone'] = $staff->getPhone();
            $data[$i]['address'] = $staff->getAddress();
            $data[$i]['status'] = $staff->getStatus();
            $data[$i]['department'] = $staff->getDepartment()->getName();
            $i++;
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/staff/{pin}")
     * @Method("GET")
     * @param int $pin
     * @return JsonResponse
     */
    public function getStaffAction(int $pin)
    {
        $staffs = $this->getDoctrine()->getRepository('AppBundle:Staff')
            ->getStaffByPin($pin);

        $data = [];
        $i = 0;

        /** @var Staff $staff */
        foreach ($staffs as $staff) {
            $data[$i]['id'] = $staff->getId();
            $data[$i]['surname'] = $staff->getSurname();
            $data[$i]['name'] = $staff->getName();
            $data[$i]['patronymic'] = $staff->getPatronymic();
            $data[$i]['pin'] = $staff->getPin();
            $data[$i]['email'] = $staff->getEmail();
            $data[$i]['phone'] = $staff->getPhone();
            $data[$i]['address'] = $staff->getAddress();
            $data[$i]['status'] = $staff->getStatus();
            $data[$i]['department'] = $staff->getDepartment()->getName();
            $i++;
        }

        return new JsonResponse($data);
    }
}
