<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Department;
use AppBundle\Entity\Staff;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $department1 = new Department();
        $department2 = new Department();
        $department3 = new Department();
        $department4 = new Department();

        $department1->setName('Бишкек');
        $department2->setName('Ош');
        $department3->setName('Москва');
        $department4->setName('Антананариву');

        $manager->persist($department1);
        $manager->persist($department2);
        $manager->persist($department3);
        $manager->persist($department4);

        $staffs = [];

        for ($i = 0; $i < 20; $i++) {
            $staffs[] = new Staff();
        }

        $k = 0;
        /** @var Staff $staff */
        foreach ($staffs as $staff) {
            $staff->setName('Тестовый'.$k);
            $staff->setsurName('Пользователь'.$k);
            $staff->setPatronymic('Тестович'.$k);
            $staff->setPin(1300210+$k);
            $staff->setEmail('tester'.$k.'@mail.ru');
            $staff->setPhone(996555155+$k);
            $staff->setAddress('ул. Киевская, дом '.$k);
            $statuses = array(
                'назначен',
                'освобожден',
            );
            $key = array_rand($statuses);
            $staff->setStatus($statuses[$key]);
            $departments = array(
                $department1,
                $department2,
                $department3,
                $department4
            );
            $key = array_rand($departments);
            $staff->setDepartment($departments[$key]);
            $manager->persist($staff);
            $k++;
        }

        $manager->flush();
    }
}